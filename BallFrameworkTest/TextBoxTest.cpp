#include "pch.h"
#include "CppUnitTest.h"
#include"../BallFramework/TextBox.h"
#include"../BallFramework/TextBox.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TextBoxTest
{
	TEST_CLASS(TextBoxTest)
	{
	public:

		TEST_METHOD(TestText)
		{
			TextBox textBox;
			sf::Font font;
			if (!font.loadFromFile("arial.ttf"))
			{
				std::cout << "Font not loaded\n";
			}
			textBox.SetText("text test", sf::Color::White, { 90, 60 }, font);
			Assert::AreEqual<std::string>("text test", textBox.GetDescription().getString());
		}
	};
}