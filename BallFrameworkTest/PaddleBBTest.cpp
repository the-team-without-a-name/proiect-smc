#include "pch.h"
#include "CppUnitTest.h"
#include"../BallFramework/PaddleBB.h"
#include"../BallFramework/PaddleBB.cpp"
#include"../BallFramework/Paddle.h"
#include"../BallFramework/Paddle.cpp"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PaddleBBTest
{
	TEST_CLASS(PaddleBBTest)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			std::pair<int, int> coord(550, 750); 
			PaddleBB paddle(110, 25, coord, sf::Color::Red);
			Assert::IsTrue(paddle.GetColor()==sf::Color::Red);
		}

		TEST_METHOD(TestResize)
		{
			std::pair<int, int> coord(550, 750);
			PaddleBB paddle(110, 25, coord, sf::Color::Red);

			sf::Vector2f size(paddle.GetObject().getSize().x + 12 * paddle.GetObject().getSize().x / 2, paddle.GetObject().getSize().y);
			paddle.ChangeSize(12);

			Assert::IsTrue(paddle.GetObject().getSize()== size);
		}
	};
}