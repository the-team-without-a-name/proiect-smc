#include "pch.h"
#include "CppUnitTest.h"
#include"../BallFramework/Ball.h"
#include"../BallFramework/Ball.cpp"
#include"../BallFramework/Player.h"
#include"../BallFramework/Player.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BallTest
{
	TEST_CLASS(BallTest)
	{
	public:

		TEST_METHOD(TestSpeed)
		{
			Ball ball(2.3, 3.5);
			ball.SetSpeed(1.5);
			ball.SetSpeed(2);
			Assert::IsFalse(ball.GetSpeed() == 1.5);

		}

		TEST_METHOD(TestOutlineColor)
		{
			Ball ball(2.3, 3.5);
			ball.SetOutlineColor(sf::Color::Cyan);
			Assert::IsTrue(ball.GetObject().getOutlineColor() == sf::Color::Cyan);

		}

		TEST_METHOD(TestConstructor)
		{
			Ball ball(2.3, 3.5, 0.5, 0.9);
			Assert::AreEqual<float>(ball.GetDirX(), 0.5);	

		}
	};
}