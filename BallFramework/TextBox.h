#pragma once
#include <SFML/Graphics.hpp>
#include <sstream>

constexpr auto DELETE_KEY = 8;
constexpr auto ENTER_KEY = 13;
constexpr auto ESCAPE_KEY = 27;

class TextBox
{
public:
	TextBox() = default;
	TextBox(int size, sf::Color color, bool sel, int limit, sf::Font& FONT);

	void SetPosition(sf::Vector2f pos);
	void SetLimit(int lim);
	void SetSelected(bool sel);
	void SetText(const std::string& text, sf::Color color, sf::Vector2f pos, sf::Font& FONT);

	std::string GetText() const;
	sf::Text GetDescription() const;
	bool GetSelected() const;

	void Draw(sf::RenderWindow& window) const;
	void TypedOn(sf::Event input);
	bool IsPressed(sf::Vector2f pos) const;

private:
	void InputLogic(int charTyped);
	void DeleteLastChar();

	sf::Text m_textBox;
	sf::Text m_text;
	sf::RectangleShape m_box;
	std::ostringstream m_stream;

	bool m_isSelected = false;
	int m_limit;
	static const int CHARACTER_SIZE = 20;
};