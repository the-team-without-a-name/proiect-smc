#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <iostream>
#include <math.h>
#include "Ball.h"

class Brick
{
public:
	enum class Bonus {
		NONE,
		NEW_BALL,
		INCREASE_BALL_SPEED,
		DECREASE_BALL_SPEED,
		INCREASE_PADDLE_SIZE,
		DECREASE_PADDLE_SIZE
	};

	enum class CollisionType {
		NONE,
		VERTICAL,
		HORIZONTAL
	};

	Brick() = default;
	Brick(sf::Vector2f m_coord, int16_t WIDTH, int16_t HEIGHT, sf::Color color);
	Brick(sf::Vector2f m_coord, int16_t WIDTH, int16_t HEIGHT);

	Bonus GetBonus() const;
	sf::RectangleShape GetRectangle() const;
	sf::Color GetColor()const;

	void SetBonus(const Bonus& bonus);
	void SetColor(sf::Color color);

	void Draw(sf::RenderWindow& window) const;
	const Brick::CollisionType Collision(Ball& ball, bool opponents) const;

protected:
	sf::RectangleShape m_rectangle;

private:
	Bonus m_bonus;
};