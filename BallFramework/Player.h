#pragma once
#include <iostream>
#include <string>
#include <fstream>

class Player
{
public:
	Player();
	Player(std::string name);
	Player(std::string name, uint8_t level, uint8_t winGames, uint8_t numOfGames);

	Player(const Player& other);
	Player(Player&& other) noexcept;

	Player& operator=(const Player& other);
	Player& operator=(Player&& other) noexcept;

	friend std::istream& operator>>(std::istream&, Player&);
	friend std::ostream& operator<<(std::ostream&, Player&);

	std::string GetName() const;
	uint16_t GetLevel() const;
	uint16_t GetNumGames() const;
	uint16_t GetWinGames() const;
	uint16_t GetLives() const;

	void SetName(std::string name);
	void SetLevel(uint16_t level);
	void SetWinGames(uint16_t winGames);
	void SetNumGames(uint16_t numGames);
	void SetLives(uint16_t lives);

private:
	std::string m_name;
	uint16_t m_winGames;
	uint16_t m_numOfGames;
	uint16_t m_lives;
	uint16_t m_level;
};