#pragma once
#include <SFML/Graphics.hpp>
#include "../Logging/Logging.h"

class Button
{
public:
	Button() = default;
	Button(const std::string& imageName, sf::RenderWindow& window, const sf::Vector2f& pos, sf::Font& FONT, std::shared_ptr<Logger> logger);

	const sf::Sprite& GetSprite() const;
	const sf::Text& GetText() const;

	void SetText(const std::string& text, sf::Color color, sf::Vector2f pos, sf::Font& FONT);

private:
	sf::Texture m_texture;
	sf::Text m_text;
	sf::Sprite m_sprite;
	static const int CHARACTER_SIZE = 24;
};