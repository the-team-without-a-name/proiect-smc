#include "GameP.h"
#include <SFML/Graphics.hpp>

const int GameP::MAX_SCORE = 5;

GameP::GameP() :Game()
{
	m_opponents = true;

	Ball m_ball(600.f, 400.f);
	m_level.AddBall(m_ball);
	std::pair<int, int> coord(1160, 350);
	m_player1.second = PaddleP(25, 110, coord, LevelP::COLOR);
	std::pair<int, int> coord2(15, 170);

	m_player2.second = PaddleP(25, 490, coord2, { 26, 26, 255 });
	m_level.SetLevel("BrickPong.txt");
}

void GameP::Run()
{
	bool gameOver = false;
	while (m_window.isOpen())
	{
		if (gameOver == false)
			Update();

		ProcessEvents();
		Render(gameOver);
	}

	if (player1Won)
	{
		m_player1.first.SetWinGames(m_player1.first.GetWinGames() + 1);
		m_player1.first.SetNumGames(m_player1.first.GetNumGames() + 1);
		m_player2.first.SetNumGames(m_player2.first.GetNumGames() + 1);
	}
	else
		if (player2Won)
		{
			m_player2.first.SetWinGames(m_player2.first.GetWinGames() + 1);
			m_player2.first.SetNumGames(m_player2.first.GetNumGames() + 1);
			m_player1.first.SetNumGames(m_player1.first.GetNumGames() + 1);
		}
}

void GameP::SetPlayer1Name(const std::string& name, std::shared_ptr<Logger> logger)
{
	m_player1.first.SetName(name);
	Statistics::Verif(m_player1.first, logger);
}

void GameP::SetPlayer2Name(const std::string& name, std::shared_ptr<Logger> logger)
{
	m_player2.first.SetName(name);
	Statistics::Verif(m_player2.first, logger);
}

void GameP::UpdatePlayer1(std::shared_ptr<Logger> logger)
{
	Statistics::Update(m_player1.first, logger);
}

void GameP::UpdatePlayer2(std::shared_ptr<Logger> logger)
{
	Statistics::Update(m_player2.first, logger);
}

void GameP::ProcessEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed: HandlePlayerInput(event.key.code, true); break;
		case sf::Event::KeyReleased: HandlePlayerInput(event.key.code, false); break;
		case sf::Event::Closed: m_window.close(); break;
		}
	}
}

void GameP::Update()
{
	sf::Vector2f movement1(0.f, 0.f), movement2(0.f, 0.f);

	if (m_player1.second.GetUpMove()) movement1.y -= Paddle::SPEED;
	if (m_player1.second.GetDownMove()) movement1.y += Paddle::SPEED;
	m_player1.second.Move(movement1);

	m_level.UpdateLevel(m_player1.first, m_player1.second, m_score_player1, m_score_player2);
	m_level.UpdateLevel(m_player2.first, m_player2.second, m_score_player1, m_score_player2);
}

void GameP::Render(bool& gameOver)
{
	m_window.clear();
	m_level.Draw(m_window);
	m_window.draw(m_player1.second.GetObject());
	m_window.draw(m_player2.second.GetObject());
	m_level.DrawBricks(m_window);

	sf::Font font;
	font.loadFromFile("arial.ttf");

	sf::Text text, score1, score2;
	std::string status, scoreplayer1, scoreplayer2;
	scoreplayer1 += std::to_string(m_score_player1);
	scoreplayer2 += std::to_string(m_score_player2);
	if (m_score_player1 == MAX_SCORE)
	{
		m_window.clear();
		status = "You lose!";
		text.setString(status);
		text.setFillColor(sf::Color::Green);
		text.setPosition({ 400, 300 });
		text.setCharacterSize(100);
		text.setFont(font);
		m_window.draw(text);
		player1Won = true;
		m_window.display();
		gameOver = true;
	}

	else if (m_score_player2 == MAX_SCORE)
	{
		m_window.clear();
		status = "You won!";
		text.setString(status);
		text.setFillColor(sf::Color::Green);
		text.setPosition({ 400, 300 });
		text.setCharacterSize(100);
		text.setFont(font);
		m_window.draw(text);

		player2Won = true;
		m_window.display();
		gameOver = true;
	}
	else
	{
		score1.setString(scoreplayer1);
		score2.setString(scoreplayer2);
		score1.setFillColor(sf::Color::Yellow);
		score2.setFillColor(sf::Color::Yellow);
		score1.setPosition({ 100, 700 });
		score2.setPosition({ 1100, 700 });
		score1.setCharacterSize(30);
		score2.setCharacterSize(30);
		score1.setFont(font);
		score2.setFont(font);
		m_window.draw(score1);
		m_window.draw(score2);
		m_window.display();
	}
}

void GameP::HandlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if (key == sf::Keyboard::Up) m_player1.second.SetUpMove(isPressed);
	else if (key == sf::Keyboard::Down)  m_player1.second.SetDownMove(isPressed);
}