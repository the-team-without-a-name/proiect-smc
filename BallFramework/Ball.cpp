#include "Ball.h"
#include "PaddleBB.h"
#include "Player.h"
#include <iostream>
#include "Game.h"
#include <random>

const float Ball::DEFAULT_SPEED = 0.6f;

Ball::Ball(float coordX, float coordY)
{
	std::random_device device;
	std::mt19937 generator_random(device());

	m_ball.setPosition({ coordX, coordY });
	m_ball.setRadius(10.f);
	m_ball.setFillColor(sf::Color::Green);

	const int v[] = { -1, 1 };
	m_dirX = v[generator_random() % 2];
	m_dirY = v[generator_random() % 2];
}

Ball::Ball(float coordX, float coordY, float dirX, float dirY)
{
	m_ball.setPosition({ coordX, coordY });
	m_ball.setRadius(10.f);
	m_ball.setFillColor(sf::Color::Transparent);
	m_ball.setOutlineThickness(-2);
	m_ball.setOutlineColor(sf::Color::Blue);

	m_dirX = dirX;
	m_dirY = dirY;
	m_paddleColor = sf::Color::Black;
}

void Ball::SetSpeed(float speed)
{
	m_speed = speed;
}

void Ball::SetOutlineColor(sf::Color color)
{
	m_ball.setOutlineColor(color);
}

float Ball::GetSpeed() const
{
	return m_speed;
}

float Ball::GetDirX() const
{
	return m_dirX;
}

sf::CircleShape Ball::GetObject() const
{
	return m_ball;
}

sf::Color Ball::GetPaddleColor() const
{
	return m_paddleColor;
}

void Ball::WallCollision()
{
	if (m_ball.getPosition().y + m_ball.getRadius() > Game::HEIGHT || m_ball.getPosition().y < 0)
		ReboundY();

	if (m_ball.getPosition().x + m_ball.getRadius() > Game::WIDTH || m_ball.getPosition().x < 0)
		ReboundX();

	m_ball.move(m_dirX * m_speed, m_dirY * m_speed);
}

bool Ball::IsWallCollisionBB()
{
	if (m_ball.getPosition().y + m_ball.getLocalBounds().height >= Game::HEIGHT)
		return true;

	WallCollision();
	return false;
}

void Ball::ResetBallBB(Player& player)
{
	m_ball.setPosition(Game::WIDTH / 2, Game::HEIGHT / 2);
	ReboundY();
	player.SetLives(player.GetLives() - 1);
}

bool Ball::IsWallCollisionPong()
{
	if (m_ball.getPosition().x <= 0 || m_ball.getPosition().x + m_ball.getLocalBounds().width > Game::WIDTH)
		return true;

	WallCollision();
	return false;
}

void Ball::ResetBallPong(int& scorePlayer1, int& scorePlayer2)
{
	if (m_ball.getPosition().x <= 0)
		scorePlayer2++;

	if (m_ball.getPosition().x + m_ball.getLocalBounds().width > Game::WIDTH)
		scorePlayer1++;

	m_ball.setPosition(Game::WIDTH / 2, Game::HEIGHT / 2);
	ReboundX();
}


void Ball::PaddleCollision(Paddle& paddle)
{
	sf::FloatRect paddleBounds = paddle.GetObject().getGlobalBounds();
	sf::FloatRect ballBounds = m_ball.getGlobalBounds();

	if (paddleBounds.intersects(ballBounds)) {

		if (paddleBounds.contains(ballBounds.left + ballBounds.width / 2, ballBounds.top))
			ReboundX();

		else
			if (paddleBounds.contains(ballBounds.left, ballBounds.top + ballBounds.height / 2)
				|| paddleBounds.contains(ballBounds.left + ballBounds.width, ballBounds.top + ballBounds.height / 2))
				ReboundY();

		m_paddleColor = paddle.GetColor();
	}
}

void Ball::ReboundX()
{
	m_dirX *= -1;
	m_ball.move(m_dirX * m_speed, m_dirY * m_speed);
}

void Ball::ReboundY()
{
	m_dirY *= -1;
	m_ball.move(m_dirX * m_speed, m_dirY * m_speed);
}