#pragma once
#include "Brick.h"

class BrickBB : public Brick
{
public:
	BrickBB() = default;
	BrickBB(sf::Vector2f m_coord, sf::Color color);
	BrickBB(sf::Vector2f m_coord);

	static const int16_t WIDTH = 90;
	static const int16_t HEIGHT = 40;
};