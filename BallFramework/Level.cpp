#include "Level.h"
#include "Game.h"
#include <memory>

void Level::AddBall(Ball& ball)
{
	m_balls.push_back(ball);
}

void Level::Draw(sf::RenderWindow& window) const
{
	for (const auto& elem : m_balls)
		window.draw(elem.GetObject());
	for (const auto& elem : m_bonuses)
		window.draw(elem.first.GetObject());
}

void Level::UpdateBonuses(Paddle& paddle)
{
	sf::FloatRect paddleBounds = paddle.GetObject().getGlobalBounds();
	sf::FloatRect bonusBounds;

	for (auto elem = m_bonuses.begin(); elem != m_bonuses.end(); ++elem) {
		bonusBounds = elem->first.GetObject().getGlobalBounds();

		if (paddleBounds.intersects(bonusBounds)) {

			std::chrono::system_clock::time_point timeNow = std::chrono::system_clock::now();
			std::time_t timeStart = std::chrono::system_clock::to_time_t(timeNow);

			switch (elem->second)
			{
			case Brick::Bonus::NEW_BALL:
			{
				Ball newBall(Game::WIDTH / 2, Game::HEIGHT / 2);
				m_balls.push_back(newBall);
				break;
			}

			case Brick::Bonus::INCREASE_BALL_SPEED:
			{
				for (auto& ball : m_balls)
					ball.SetSpeed(ball.GetSpeed() + ball.GetSpeed() / 2);

				if (paddle.GetColor() == sf::Color::Color(102, 0, 255))
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::INCREASE_BALL_SPEED, timeStart, true));
				else
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::INCREASE_BALL_SPEED, timeStart, false));
				break;
			}

			case Brick::Bonus::DECREASE_BALL_SPEED:
			{
				for (auto& ball : m_balls)
					ball.SetSpeed(ball.GetSpeed() - ball.GetSpeed() / 2);

				if (paddle.GetColor() == sf::Color::Color(102, 0, 255))
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::DECREASE_BALL_SPEED, timeStart, true));
				else
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::DECREASE_BALL_SPEED, timeStart, false));
				break;
			}

			case Brick::Bonus::INCREASE_PADDLE_SIZE:
			{
				paddle.ChangeSize(1);

				if (paddle.GetColor() == sf::Color::Color(102, 0, 255))
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::INCREASE_PADDLE_SIZE, timeStart, true));
				else
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::INCREASE_PADDLE_SIZE, timeStart, false));
				break;
			}

			case Brick::Bonus::DECREASE_PADDLE_SIZE:
			{
				paddle.ChangeSize(-1);

				if (paddle.GetColor() == sf::Color::Color(102, 0, 255))
					m_bonusTime.push_back(std::make_tuple(Brick::Bonus::DECREASE_PADDLE_SIZE, timeStart, true));
				else
					m_bonusTime.emplace_back(std::tuple(Brick::Bonus::DECREASE_PADDLE_SIZE, timeStart, false));
				break;
			}
			}
			
			auto copyElem = elem;
			if (++elem == m_bonuses.end()) {
				m_bonuses.erase(copyElem);
				break;
			}
			else
				if (m_bonuses.size() > 1) {
					m_bonuses.erase(copyElem);
				}
				else {
					m_bonuses.clear();
					break;
				}
		}
	}

	for (auto elem = m_bonusTime.begin(); elem != m_bonusTime.end(); ++elem)
	{
		auto [elemBonus, elemTime, elemBool] = *elem;
		auto timeEnd = std::chrono::system_clock::now();
		auto timeStart = std::chrono::system_clock::from_time_t(elemTime);

		auto timeTotal = std::chrono::duration_cast<std::chrono::seconds> (timeEnd - timeStart);
		const std::chrono::seconds timeBonus(TIMEBONUS);

		bool isRightPaddle = true;
		if (timeTotal > timeBonus)
		{
			switch (elemBonus)
			{

			case Brick::Bonus::INCREASE_BALL_SPEED:
			{
				for (auto& ball : m_balls)
					ball.SetSpeed((ball.GetSpeed() - ball.GetSpeed() / 2) + (ball.GetSpeed() - ball.GetSpeed() / 2) / 3);
				break;
			}

			case Brick::Bonus::DECREASE_BALL_SPEED:
			{
				for (auto& ball : m_balls)
					ball.SetSpeed((ball.GetSpeed() + ball.GetSpeed() / 2) + (ball.GetSpeed() + ball.GetSpeed() / 2) / 3);
				break;
			}

			case Brick::Bonus::INCREASE_PADDLE_SIZE:
			{
				if (elemBool == true && paddle.GetColor() == sf::Color::Color(102, 0, 255))
					paddle.ChangeSize(-0.6);
				else
					if (elemBool == false && paddle.GetColor() == sf::Color::Color(209, 26, 255))
						paddle.ChangeSize(-0.6);
					else
						isRightPaddle = false;
				break;
			}

			case Brick::Bonus::DECREASE_PADDLE_SIZE:
			{
				if (elemBool == true && paddle.GetColor() == sf::Color::Color(102, 0, 255))
					paddle.ChangeSize(1.9);
				else
					if (elemBool == false && paddle.GetColor() == sf::Color::Color(209, 26, 255))
						paddle.ChangeSize(1.9);
					else
						isRightPaddle = false;
				break;
			}
			}

			if (isRightPaddle)
			{
				auto copyElem = elem;
				if (++elem == m_bonusTime.end()) {
					m_bonusTime.erase(copyElem);
					break;
				}
				else
					if (m_bonusTime.size() > 1) {
						m_bonusTime.erase(copyElem);
					}
					else {
						m_bonusTime.clear();
						break;
					}
			}
		}
	}
}