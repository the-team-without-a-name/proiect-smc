#pragma once
#include <SFML/Graphics.hpp>
#include "Ball.h"
#include "Player.h"
#include "Statistics.h"
#include "Brick.h"

class Game
{
public:
	Game();
	virtual void Run() = 0;
	virtual void SetPlayer1Name(const std::string& name, std::shared_ptr<Logger> logger) = 0;
	virtual void SetPlayer2Name(const std::string& name, std::shared_ptr<Logger> logger) = 0;
	virtual void UpdatePlayer1(std::shared_ptr<Logger> logger) = 0;
	virtual void UpdatePlayer2(std::shared_ptr<Logger> logger) = 0;

	static const int16_t HEIGHT = 800;
	static const int16_t WIDTH = 1200;

protected:
	virtual void ProcessEvents() = 0;
	virtual void Update() = 0;
	virtual void Render(bool& gameOver) = 0;
	virtual void HandlePlayerInput(sf::Keyboard::Key key, bool isPressed) = 0;

	sf::RenderWindow m_window;
	bool m_multiplayer;
	bool m_opponents;
};