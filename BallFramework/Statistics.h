#pragma once
#include <fstream>
#include "Player.h"
#include "../Logging/Logging.h"

class Statistics
{
public:
	static void Verif(Player& player, std::shared_ptr<Logger> logger);
	static void Update(const Player& player, std::shared_ptr<Logger> logger);
	static const std::string FILE;
};