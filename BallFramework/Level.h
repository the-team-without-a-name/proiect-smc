#pragma once
#include "Brick.h"
#include <fstream>
#include <list>
#include "Ball.h"
#include <vector>
#include "Paddle.h"
#include <chrono>
#include <tuple>
#include <iostream>

class Level
{
public:

	virtual void SetLevel(std::string fileName, bool multiplayer = 0) = 0;
	void AddBall(Ball& ball);
	void Draw(sf::RenderWindow& window) const;
	static const int16_t TIMEBONUS = 10;

protected:
	void UpdateBonuses(Paddle& paddle);

	std::list<Ball> m_balls;
	std::list<std::pair<Ball, Brick::Bonus>> m_bonuses;
	std::list<::std::tuple <Brick::Bonus, time_t, bool>> m_bonusTime;
};