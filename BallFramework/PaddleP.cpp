#include "PaddleP.h"
#include "Paddle.h"
#include "Game.h"
#include <SFML/Graphics.hpp>

PaddleP::PaddleP(float width, float lenght, std::pair<int, int> coord, sf::Color color)
	:Paddle(width, lenght, coord, color)
{
	m_paddle.setFillColor(color);
}

void PaddleP::Move(sf::Vector2f movement)
{
	sf::Vector2f coord = m_paddle.getPosition();
	if (coord.y + m_paddle.getSize().y < Game::HEIGHT && coord.y > 0)
	{
		m_paddle.move(movement);
	}
	else
		if (coord.y + m_paddle.getSize().y >= Game::HEIGHT && movement.y != Paddle::SPEED)
		{
			m_paddle.move(movement);
		}
		else
			if (coord.y <= 0 && movement.y != Paddle::SPEED * (-1))
			{
				m_paddle.move(movement);
			}
}

void PaddleP::ChangeSize(float size)
{
	m_paddle.setSize({ m_paddle.getSize().x, m_paddle.getSize().y + size * m_paddle.getSize().y / 2 });
}

bool PaddleP::GetUpMove() const
{
	return m_movingUp;
}

bool PaddleP::GetDownMove() const
{
	return m_movingDown;
}

void PaddleP::SetUpMove(bool up)
{
	m_movingUp = up;
}
void PaddleP::SetDownMove(bool down)
{
	m_movingDown = down;
}