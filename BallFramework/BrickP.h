#pragma once
#include "Brick.h"

class BrickP :public Brick
{
public:
	BrickP() = default;
	BrickP(sf::Vector2f m_coord);

	static const int16_t WIDTH = 40;
	static const int16_t HEIGHT = 90;
};