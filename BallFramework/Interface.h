#pragma once
#include <SFML/Graphics.hpp>

class Interface
{
public:

	class Button {
	public:
		Button() = default;
		Button(const std::string& imageName, sf::RenderWindow& window, const sf::Vector2f& pos);

		const sf::Sprite& GetSprite() const;
		const sf::Text& GetText() const;

		void SetText(const std::string& text, sf::Color color, sf::Vector2f pos);

	private:
		sf::Texture m_texture;
		sf::Text m_text;
		sf::Sprite m_sprite;

		sf::Font FONT;
		static const int CHARACTER_SIZE = 24;
	};

private:
	
};

