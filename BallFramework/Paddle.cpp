#include "Paddle.h"
#include <SFML/Graphics.hpp>

const float Paddle::SPEED = 2.0f;

Paddle::Paddle(float width, float lenght, std::pair<int, int> coord, sf::Color color)
{
	m_paddle.setSize(sf::Vector2f(width, lenght));
	m_paddle.setPosition(coord.first, coord.second);
}

sf::RectangleShape Paddle::GetObject() const
{
	return m_paddle;
}

sf::Color Paddle::GetColor() const
{
	return m_paddle.getFillColor();
}