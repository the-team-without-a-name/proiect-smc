#include "Statistics.h"

const std::string Statistics::FILE = "Players.txt";

void Statistics::Verif(Player& player, std::shared_ptr<Logger> logger)
{
	Player p;
	std::ifstream f(FILE);

	if (f.is_open())
	{
		while (f >> p) {
			if (player.GetName() == p.GetName())
			{
				player = p;
				break;
			}
		}
		f.clear();
		f.close();
	}
	else
		logger->log("File can't open", Logger::Level::Error);
}

void Statistics::Update(const Player& player, std::shared_ptr<Logger> logger)
{
	Player p;
	std::fstream f(FILE);
	bool find = false;
	int pos = -1;

	if (f.is_open())
	{
		while (!f.eof()) {
			pos = f.tellg();
			f >> p;
			if (const_cast<Player&>(player).GetName() == p.GetName())
			{
				find = true;
				if (pos > 0)
					pos += 2;
				f.clear();
				f.seekg(pos);
				f << const_cast<Player&>(player);
				break;
			}
		}
		f.clear();
		f.close();

		if (find == false)
		{
			f.open(FILE, std::ios::app);
			f << const_cast<Player&>(player);
			f.clear();
			f.close();
		}
	}
	else
		logger->log("File can't open", Logger::Level::Error);
}