#include "Button.h"
#include <iostream>

Button::Button(const std::string& imageName, sf::RenderWindow& window, const sf::Vector2f& pos, sf::Font& FONT, std::shared_ptr<Logger> logger)
{
	if (!m_texture.loadFromFile(imageName))
	{
		logger->log("Image not loaded", Logger::Level::Error);
	}
	m_sprite.setPosition(pos);
	m_sprite.setTexture(m_texture);
}

const sf::Sprite& Button::GetSprite() const
{
	return m_sprite;
}

const sf::Text& Button::GetText() const
{
	return m_text;
}

void Button::SetText(const std::string& text, sf::Color color, sf::Vector2f pos, sf::Font& FONT)
{
	m_text.setString(text);
	m_text.setFillColor(color);
	m_text.setPosition(pos);
	m_text.setCharacterSize(CHARACTER_SIZE);
	m_text.setFont(FONT);
}