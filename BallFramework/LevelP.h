#pragma once
#include"Level.h"
#include"BrickP.h"
#include <random>
#include <chrono>

class LevelP : public Level
{
public:
	void SetLevel(std::string fileName, bool multiplayer = 0);
	void DrawBricks(sf::RenderWindow& window);
	void Update(Ball& ball);
	void UpdateLevel(Player& player, Paddle& paddle, int& scorePlayer1, int& scorePlayer2);

	static const sf::Color COLOR;

private:
	std::list<BrickP> m_brickList;
	
};