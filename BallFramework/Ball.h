#pragma once
#include <SFML/Graphics.hpp>
#include "Paddle.h"
#include "Player.h"
#include "PaddleBB.h"
#include <sstream>

class Ball
{
public:
	Ball() = default;
	Ball(float coordX, float coordY);
	Ball(float coordX, float coordY, float dirX, float dirY);

	void SetSpeed(float speed);
	void SetOutlineColor(sf::Color color);

	float GetSpeed() const;
	float GetDirX() const;
	sf::CircleShape GetObject() const;
	sf::Color GetPaddleColor() const;

	void WallCollision();
	void PaddleCollision(Paddle& paddle);
	void ReboundX();
	void ReboundY();

	bool IsWallCollisionBB();
	void ResetBallBB(Player& player);

	bool IsWallCollisionPong();
	void ResetBallPong(int& scorePlayer1, int& scorePlayer2);

	static const float DEFAULT_SPEED;

private:
	sf::CircleShape m_ball;
	sf::Color m_paddleColor;
	float m_dirX = 1;
	float m_dirY = 1;
	float m_speed = DEFAULT_SPEED;
};