#pragma once
#include"Level.h"
#include"BrickBB.h"
#include <random>
#include <chrono>

class LevelBB : public Level
{
public:
	void SetLevel(std::string fileName, bool multiplayer = 0);
	void DrawBricks(sf::RenderWindow& window);
	void Update(Ball& ball, bool opponents);
	void UpdateLevel(Player& player, bool multiplayer, bool opponents, Paddle& paddle);
	bool IsEmpty();

	void VerifiyWin(int& nrBricksP1, int& nrBricksP2, const sf::Color& color);
private:
	std::list<BrickBB> m_brickList;
};