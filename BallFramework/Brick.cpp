#include "Brick.h"
#include"BrickBB.h"

Brick::Brick(sf::Vector2f m_coord, int16_t WIDTH, int16_t HEIGHT, sf::Color color)
{
	m_rectangle.setSize(sf::Vector2f(WIDTH, HEIGHT));
	m_rectangle.setPosition(m_coord.x, m_coord.y);
	m_rectangle.setFillColor(color);
}

Brick::Brick(sf::Vector2f m_coord, int16_t WIDTH, int16_t HEIGHT)
{
	m_rectangle.setSize(sf::Vector2f(WIDTH, HEIGHT));
	m_rectangle.setPosition(m_coord.x, m_coord.y);
	m_rectangle.setFillColor(sf::Color::Color(102, 0, 255));
}

void Brick::Draw(sf::RenderWindow& window) const
{
	window.draw(m_rectangle);
}

const Brick::CollisionType Brick::Collision(Ball& ball, bool opponents) const
{
	sf::FloatRect boundingBox = ball.GetObject().getGlobalBounds();
	sf::FloatRect bounds = m_rectangle.getGlobalBounds();
	sf::Vector2f center(boundingBox.left + boundingBox.width / 2, boundingBox.top + boundingBox.height / 2);

	if (bounds.intersects(boundingBox))
	{
		if (bounds.contains(center.x, boundingBox.top)
			|| bounds.contains(center.x, boundingBox.top + boundingBox.height)) {

			if (ball.GetPaddleColor() != m_rectangle.getFillColor() && opponents)
				ball.ReboundY();
			else
				return Brick::CollisionType::VERTICAL;
		}
		else
			if (bounds.contains(boundingBox.left, center.y)
				|| bounds.contains(boundingBox.left + boundingBox.width, center.y)) {
				if (ball.GetPaddleColor() != m_rectangle.getFillColor() && opponents)
					ball.ReboundX();
				else
					return Brick::CollisionType::HORIZONTAL;
			}
	}
	return Brick::CollisionType::NONE;
}

Brick::Bonus Brick::GetBonus() const
{
	return m_bonus;
}

void Brick::SetBonus(const Bonus& bonus)
{
	m_bonus = bonus;
}

sf::RectangleShape Brick::GetRectangle() const
{
	return m_rectangle;
}

sf::Color Brick::GetColor() const
{
	return m_rectangle.getFillColor();
}

void Brick::SetColor(sf::Color color)
{
	m_rectangle.setFillColor(color);
}