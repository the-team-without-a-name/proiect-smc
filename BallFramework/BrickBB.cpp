#include "BrickBB.h"

BrickBB::BrickBB(sf::Vector2f m_coord, sf::Color color) :Brick::Brick(m_coord, BrickBB::WIDTH, BrickBB::HEIGHT, color)
{}

BrickBB::BrickBB(sf::Vector2f m_coord) : Brick::Brick(m_coord, BrickBB::WIDTH, BrickBB::HEIGHT)
{}