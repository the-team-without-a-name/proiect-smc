#include "LevelBB.h"
#include "Game.h"

void LevelBB::SetLevel(std::string fileName, bool multiplayer)
{
	std::random_device device;
	std::mt19937 generator(device());
	std::vector<int> randomBonus(10);

	std::fill(randomBonus.begin(), randomBonus.begin() + 5, 0);
	int size = 1;
	std::for_each(randomBonus.begin() + 5, randomBonus.end(), [&size](int& elem)
	{
		elem = size;
		size++;
	});

	sf::Vector2f m_coord;
	std::ifstream fis(fileName);

	int numberBricks;
	fis >> numberBricks;
	for (int index = 0; index < numberBricks; index++)
	{
		fis >> m_coord.x >> m_coord.y;
		BrickBB brick(m_coord);
		if (multiplayer)
			if (index % 2 == 0)
				brick.SetColor({ 102, 0, 255 });
			else
				brick.SetColor(sf::Color::Color(209, 26, 255));

		int random = generator() % randomBonus.size();
		Brick::Bonus bonus = static_cast <Brick::Bonus>(randomBonus[random]);
		brick.SetBonus(bonus);

		m_brickList.push_back(brick);
	}
}

void LevelBB::DrawBricks(sf::RenderWindow& window)
{
	for (auto elem : m_brickList)
		elem.Draw(window);
}

void LevelBB::Update(Ball& ball, bool opponents)
{
	sf::FloatRect ballBounds = ball.GetObject().getGlobalBounds();
	Brick::CollisionType result;

	for (auto p = m_brickList.begin(); p != m_brickList.end(); ++p) {
		result = p->Collision(ball, opponents);
		if (result == Brick::CollisionType::VERTICAL)
			ball.ReboundY();
		else
			if (result == Brick::CollisionType::HORIZONTAL)
				ball.ReboundX();
		if (result != Brick::CollisionType::NONE) {

			if (p->GetBonus() != Brick::Bonus::NONE) {
				Ball ball(p->Brick::GetRectangle().getPosition().x + BrickBB::WIDTH / 2, p->Brick::GetRectangle().getPosition().y + BrickBB::HEIGHT / 2, 0, 1);
				if (p->GetBonus() == Brick::Bonus::DECREASE_BALL_SPEED || p->GetBonus() == Brick::Bonus::DECREASE_PADDLE_SIZE)
					ball.SetOutlineColor(sf::Color::Red);
				ball.SetSpeed(ball.GetSpeed() * 1.5f);
				m_bonuses.emplace_back(ball, p->GetBonus());
			}
			m_brickList.erase(p);
			break;
		}
	}
}

void LevelBB::UpdateLevel(Player& player, bool multiplayer, bool opponents, Paddle& paddle)
{
	UpdateBonuses(paddle);

	for (auto elem = m_balls.begin(); elem != m_balls.end(); ++elem) {
		elem->PaddleCollision(paddle);
		if (elem->IsWallCollisionBB()) {
			if ((m_balls.size() > 1 && !opponents) || (m_balls.size() > 2 && opponents)) {
				auto copyElem = elem;
				if (++elem == m_balls.end()) {
					m_balls.erase(copyElem);
					break;
				}
				else
					m_balls.erase(copyElem);
			}
			else
			{
				if (opponents)
				{
					if (paddle.GetObject().getFillColor() == elem->GetPaddleColor())
						elem->ResetBallBB(player);
					if (elem->GetPaddleColor() == sf::Color::Black)
					{
						elem->ResetBallBB(player);
						player.SetLives(player.GetLives() + 1);
					}
				}
				else
					elem->ResetBallBB(player);
			}
		}
		Update(*elem, opponents);
	}

	for (auto elem = m_bonuses.begin(); elem != m_bonuses.end(); ++elem) {
		if (elem->first.IsWallCollisionBB()) {
			if (m_bonuses.size() == 1)
			{
				m_bonuses.clear();
				break;
			}
			else
			{
				auto copyElem = elem;
				if (++elem == m_bonuses.end()) {
					m_bonuses.erase(copyElem);
					break;
				}
				else
					m_bonuses.erase(copyElem);
			}
		}
	}
}

bool LevelBB::IsEmpty()
{
	return m_brickList.empty();
}

void LevelBB::VerifiyWin(int& nrBricksP1, int& nrBricksP2, const sf::Color& color)
{
	std::for_each(m_brickList.begin(), m_brickList.end(), [color, &nrBricksP1, &nrBricksP2](BrickBB const& brick)
		{
			if (brick.GetColor() == color)
				nrBricksP1++;
			else
				nrBricksP2++;
		});
}