#include "GameBB.h"

GameBB::GameBB(bool multiplayer, bool opponents) :Game()
{
	m_multiplayer = multiplayer;
	m_opponents = opponents;

	sf::Color color(102, 0, 255);
	std::pair<int, int> coord(550, 750);

	Ball ball1(600.f, 400.f);
	m_level.AddBall(ball1);

	if (multiplayer)
	{
		coord.first = 300;
		m_player2.second = PaddleBB(110, 25, coord, color);
		coord.first = 800;

		color.r = 209;
		color.g = 26;
	}

	if (m_opponents)
	{
		Ball ball2(600.f, 500.f);
		m_level.AddBall(ball2);
	}

	m_player.second = PaddleBB(110, 25, coord, color);
	m_level.SetLevel("Brick.txt", m_multiplayer);
}

void GameBB::Run()
{
	bool gameOver = false;
	while (m_window.isOpen())
	{
		if (gameOver == false)
		{
			Update();
		}
		ProcessEvents();
		Render(gameOver);
	}

	if (player1Won)
		m_player.first.SetWinGames(m_player.first.GetWinGames() + 1);
	if (player2Won)
		m_player2.first.SetWinGames(m_player2.first.GetWinGames() + 1);

	m_player.first.SetNumGames(m_player.first.GetNumGames() + 1);

	if (m_multiplayer)
		m_player2.first.SetNumGames(m_player2.first.GetNumGames() + 1);
}

void GameBB::SetPlayer2Name(const std::string& name, std::shared_ptr<Logger> logger)
{
	m_player2.first.SetName(name);
	Statistics::Verif(m_player2.first, logger);
}

void GameBB::UpdatePlayer1(std::shared_ptr<Logger> logger)
{
	Statistics::Update(m_player.first, logger);
}

void GameBB::UpdatePlayer2(std::shared_ptr<Logger> logger)
{
	Statistics::Update(m_player2.first, logger);
}

void GameBB::SetPlayer1Name(const std::string& name, std::shared_ptr<Logger> logger)
{
	m_player.first.SetName(name);
	Statistics::Verif(m_player.first, logger);
}

void GameBB::ProcessEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed: HandlePlayerInput(event.key.code, true); break;
		case sf::Event::KeyReleased: HandlePlayerInput(event.key.code, false); break;
		case sf::Event::Closed: m_window.close(); break;
		}
	}
}

void GameBB::Update()
{
	sf::Vector2f movement(0.f, 0.f);
	if (m_player.second.GetLeftMove()) movement.x -= Paddle::SPEED;
	if (m_player.second.GetRightMove()) movement.x += Paddle::SPEED;
	m_player.second.Move(movement);

	m_level.UpdateLevel(m_player.first, m_multiplayer, m_opponents, m_player.second);

	if (m_multiplayer)
	{
		movement.x = 0.f;
		movement.y = 0.f;

		if (m_player2.second.GetLeftMove()) movement.x -= Paddle::SPEED;
		if (m_player2.second.GetRightMove()) movement.x += Paddle::SPEED;
		m_player2.second.Move(movement);
		if (m_opponents)
			m_level.UpdateLevel(m_player2.first, m_multiplayer, m_opponents, m_player2.second);
		else
			m_level.UpdateLevel(m_player.first, m_multiplayer, m_opponents, m_player2.second);
	}
}

void GameBB::Render(bool& gameOver)
{
	m_window.clear();
	m_level.Draw(m_window);
	m_window.draw(m_player.second.GetObject());
	if (m_multiplayer)
		m_window.draw(m_player2.second.GetObject());
	m_level.DrawBricks(m_window);

	sf::Font font;
	font.loadFromFile("arial.ttf");

	sf::Text text;
	std::string livesP1;
	livesP1 += std::to_string(m_player.first.GetLives());

	sf::Text text2;
	std::string livesP2;
	livesP2 += std::to_string(m_player2.first.GetLives());

	if (!m_opponents)
		m_player2.first.SetLives(m_player.first.GetLives());

	int nrBricksP1 = 0, nrBricksP2 = 0;
	m_level.VerifiyWin(nrBricksP1, nrBricksP2, m_player.second.GetColor());
	
	if (nrBricksP1 == 0 && m_opponents)
	{
		m_window.clear();
		livesP1 = "Player1 won!";
		player1Won = true;
		text.setString(livesP1);
		text.setFillColor(sf::Color::Red);
		text.setPosition({ 350, 300 });
		text.setCharacterSize(100);
		text.setFont(font);
		m_window.draw(text);

		m_window.display();
		gameOver = true;
	}
	else
		if (nrBricksP2 == 0 && m_opponents)
		{
			m_window.clear();
			livesP2 = "Player2 won!";
			player2Won = true;
			text.setString(livesP2);
			text.setFillColor(sf::Color::Red);
			text.setPosition({ 350, 300 });
			text.setCharacterSize(100);
			text.setFont(font);
			m_window.draw(text);

			m_window.display();
			gameOver = true;
		}
		else
			if (m_level.IsEmpty())
			{
				m_window.clear();
				livesP1 = "You won!";
				text.setString(livesP1);
				text.setFillColor(sf::Color::Magenta);
				text.setPosition({ 400, 300 });
				text.setCharacterSize(100);
				text.setFont(font);
				m_window.draw(text);
				player1Won = true;
				if (!m_opponents)
					player2Won = true;

				m_window.display();
				gameOver = true;
			}
			else
				if (livesP1 == "0")
				{
					m_window.clear();
					if (m_opponents)
					{
						livesP1 = "Player2 won!";
						player2Won = true;
					}
					else
						livesP1 = "Game over!";

					text.setString(livesP1);
					text.setFillColor(sf::Color::Red);
					text.setPosition({ 350, 300 });
					text.setCharacterSize(100);
					text.setFont(font);
					m_window.draw(text);
					m_window.display();
					gameOver = true;
				}
				else
					if (m_opponents && livesP2 == "0")
					{
						m_window.clear();
						livesP2 = "Player1 won!";
						player1Won = true;
						text.setString(livesP2);
						text.setFillColor(sf::Color::Red);
						text.setPosition({ 350, 300 });
						text.setCharacterSize(100);
						text.setFont(font);
						m_window.draw(text);
						m_window.display();
						gameOver = true;
					}
					else
					{
						if (livesP1 == "1")
							livesP1 += " life";
						else
							livesP1 += " lives";

						if (m_opponents)
						{
							if (livesP2 == "1")
								livesP2 += " life";
							else
								livesP2 += " lives";
						}

						if (m_level.IsEmpty())
						{
							m_window.clear();
							livesP1 = "You won!";
							text.setString(livesP1);
							text.setFillColor(sf::Color::Magenta);
							text.setPosition({ 350, 300 });
							text.setCharacterSize(100);
							text.setFont(font);
							m_window.draw(text);
							player1Won = true;
							if (!m_opponents)
								player2Won = true;

							m_window.display();
							gameOver = true;
						}
						else
						{
							text.setString(livesP1);
							text.setFillColor(sf::Color::Color(178, 102, 255));
							text.setPosition({ 1100, 700 });
							text.setCharacterSize(20);
							text.setFont(font);

							if (m_opponents)
							{

								text2.setString(livesP2);
								text2.setFillColor(sf::Color::Color(178, 102, 255));
								text2.setPosition({ 45, 700 });
								text2.setCharacterSize(20);
								text2.setFont(font);
								m_window.draw(text2);
							}
							m_window.draw(text);
							m_window.display();
						}
					}
}

void GameBB::HandlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if (key == sf::Keyboard::Left) m_player.second.SetLeftMove(isPressed);
	else
		if (key == sf::Keyboard::Right) m_player.second.SetRightMove(isPressed);

	if (m_multiplayer)
	{
		if (key == sf::Keyboard::A) m_player2.second.SetLeftMove(isPressed);
		else
			if (key == sf::Keyboard::D) m_player2.second.SetRightMove(isPressed);
	}
}