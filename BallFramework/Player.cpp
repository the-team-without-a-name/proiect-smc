#include "Player.h"

Player::Player()
{
	m_name = "";
	m_winGames = 0;
	m_numOfGames = 0;
	m_lives = 3;
	m_level = 1;
}

Player::Player(std::string name)
{
	m_name = name;
	m_winGames = 0;
	m_numOfGames = 0;
	m_lives = 3;
	m_level = 1;
}

Player::Player(std::string name, uint8_t level, uint8_t winGames, uint8_t numOfGames) :m_name(name), m_winGames(winGames), m_numOfGames(numOfGames), m_level(level)
{
	m_lives = 3;
}

Player::Player(const Player& other)
{
	*this = other;
}

Player::Player(Player&& other) noexcept
{
	*this = std::move(other);
}

Player& Player::operator=(const Player& other)
{
	this->m_name = other.m_name;
	this->m_lives = other.m_lives;
	this->m_level = other.m_level;
	this->m_numOfGames = other.m_numOfGames;
	this->m_winGames = other.m_winGames;
	return *this;
}

Player& Player::operator=(Player&& other) noexcept
{
	this->m_name = other.m_name;
	this->m_lives = other.m_lives;
	this->m_level = other.m_level;
	this->m_numOfGames = other.m_numOfGames;
	this->m_winGames = other.m_winGames;
	new (&other) Player;
	return *this;
}

std::string Player::GetName() const
{
	return m_name;
}

uint16_t Player::GetLevel() const
{
	return m_level;
}

uint16_t Player::GetNumGames() const
{
	return m_numOfGames;
}

uint16_t Player::GetWinGames() const
{
	return  m_winGames;
}

uint16_t Player::GetLives() const
{
	return m_lives;
}

void Player::SetName(std::string name)
{
	m_name = name;
}

void Player::SetLevel(uint16_t level)
{
	m_level = level;
}

void Player::SetWinGames(uint16_t winGames)
{
	m_winGames = winGames;
}

void Player::SetNumGames(uint16_t numGames)
{
	m_numOfGames = numGames;
}

void Player::SetLives(uint16_t lives)
{
	m_lives = lives;
}

std::istream& operator>>(std::istream& in, Player& player)
{
	in >> player.m_name >> player.m_level >> player.m_winGames >> player.m_numOfGames;
	return in;
}

std::ostream& operator<<(std::ostream& out, Player& player)
{
	out << player.m_name << " " << player.m_level << " " << player.m_winGames << " " << player.m_numOfGames << '\n';
	return out;
}