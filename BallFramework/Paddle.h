#pragma once
#include <string>
#include <utility>
#include <SFML/Graphics.hpp>

class Paddle
{
public:
	Paddle() = default;
	Paddle(float lenght, float width, std::pair<int, int> coord, sf::Color color);

	sf::RectangleShape GetObject() const;
	sf::Color GetColor() const;

	virtual void ChangeSize(float size) = 0;

	static const float SPEED;

protected:
	sf::RectangleShape m_paddle;
};