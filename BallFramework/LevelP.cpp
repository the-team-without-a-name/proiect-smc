#include "LevelP.h"

constexpr auto numBricks = 8;
const sf::Color LevelP::COLOR = sf::Color::Color({ 153, 0, 153 });

void LevelP::SetLevel(std::string fileName, bool multiplayer)
{
	std::random_device device;
	std::mt19937 generator(device());
	std::vector<int> randomBonus(10);

	std::fill(randomBonus.begin(), randomBonus.begin() + 5, 0);
	int size = 1;
	std::for_each(randomBonus.begin() + 5, randomBonus.end(), [&size](int& elem)
	{
		elem = size;
		size++;
	});

	sf::Vector2f m_coord;
	std::ifstream fis(fileName);
	int numberBricks;
	fis >> numberBricks;
	std::vector<BrickP>m_brickListPongauxiliar;

	for (int index = 0; index < numberBricks; index++)
	{
		fis >> m_coord.x >> m_coord.y;
		BrickP brick(m_coord);
		m_brickListPongauxiliar.push_back(brick);
	}

	int index = numBricks - 1;
	while (index >= 0)
	{
		BrickP brick;
		int randomBrick = generator() % m_brickListPongauxiliar.size();
		brick = m_brickListPongauxiliar[randomBrick];

		int random = generator() % randomBonus.size();
		Brick::Bonus bonus = static_cast <Brick::Bonus>(randomBonus[random]);
		brick.SetBonus(bonus);

		m_brickList.push_back(brick);
		index--;
	}
}

void LevelP::DrawBricks(sf::RenderWindow& window)
{
	for (auto elem : m_brickList)
		elem.Draw(window);
}

void LevelP::Update(Ball& ball)
{
	sf::FloatRect ballBounds = ball.GetObject().getGlobalBounds();
	Brick::CollisionType result;

	for (auto p = m_brickList.begin(); p != m_brickList.end(); ++p) {
		result = p->Collision(ball, 0);
		if (result == Brick::CollisionType::VERTICAL)
			ball.ReboundY();
		else
			if (result == Brick::CollisionType::HORIZONTAL)
				ball.ReboundX();

		if (result != Brick::CollisionType::NONE) {
			if (p->GetBonus() != Brick::Bonus::NONE)
			{
				if (ball.GetPaddleColor() == COLOR) {
					Ball ball2(p->Brick::GetRectangle().getPosition().x + BrickP::WIDTH / 2, p->Brick::GetRectangle().getPosition().y + BrickP::HEIGHT / 2, 1, 0);
					ball2.SetSpeed(ball2.GetSpeed() * 1.5f);
					if (p->GetBonus() == Brick::Bonus::DECREASE_BALL_SPEED || p->GetBonus() == Brick::Bonus::DECREASE_PADDLE_SIZE)
						ball2.SetOutlineColor(sf::Color::Red);
					m_bonuses.emplace_back(ball2, p->GetBonus());
				}
			}
			m_brickList.erase(p);
			break;
		}
	}
}

void LevelP::UpdateLevel(Player& player, Paddle& paddle, int& scorePlayer1, int& scorePlayer2)
{
	UpdateBonuses(paddle);

	for (auto elem = m_balls.begin(); elem != m_balls.end(); ++elem) {
		elem->PaddleCollision(paddle);
		if (elem->IsWallCollisionPong()) {
			if (m_balls.size() > 1) {
				auto copyElem = elem;
				if (++elem == m_balls.end()) {
					m_balls.erase(copyElem);
					break;
				}
				else
					m_balls.erase(copyElem);
			}
			else
				elem->ResetBallPong(scorePlayer1, scorePlayer2);
		}
		Update(*elem);
	}

	for (auto elem = m_bonuses.begin(); elem != m_bonuses.end(); ++elem) {
		if (elem->first.IsWallCollisionPong()) {
			if (m_bonuses.size() == 1)
			{
				m_bonuses.clear();
				break;
			}
			else
			{
				auto copyElem = elem;
				if (++elem == m_bonuses.end()) {
					m_bonuses.erase(copyElem);
					break;
				}
				else
					m_bonuses.erase(copyElem);
			}
		}
	}
}