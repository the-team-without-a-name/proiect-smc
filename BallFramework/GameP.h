#include "Game.h"
#include "PaddleP.h"
#include "LevelP.h"

class GameP : public Game
{
public:
	GameP();
	void Run() override;
	void SetPlayer1Name(const std::string& name, std::shared_ptr<Logger> logger) override;
	void SetPlayer2Name(const std::string& name, std::shared_ptr<Logger> logger) override;
	void UpdatePlayer1(std::shared_ptr<Logger> logger) override;
	void UpdatePlayer2(std::shared_ptr<Logger> logger) override;

	static const int MAX_SCORE;

private:
	void ProcessEvents() override;
	void Update() override;
	void Render(bool& gameOver) override;
	void HandlePlayerInput(sf::Keyboard::Key key, bool isPressed) override;

	std::pair<Player, PaddleP> m_player1;
	std::pair<Player, PaddleP> m_player2;
	int m_score_player1 = 0, m_score_player2 = 0;
	bool player1Won = false;
	bool player2Won = false;
	LevelP m_level;
};