#pragma once
#include "Paddle.h"

class PaddleP : public Paddle
{
public:
	PaddleP() = default;
	PaddleP(float lenght, float width, std::pair<int, int> coord, sf::Color color);

	bool GetUpMove() const;
	bool GetDownMove() const;

	void SetUpMove(bool up);
	void SetDownMove(bool down);

	void Move(sf::Vector2f movement);
	void ChangeSize(float size) override;

private:
	bool m_movingUp = 0, m_movingDown = 0;
};