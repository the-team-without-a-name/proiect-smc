#include "GameBB.h"
#include "GameP.h"
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include "TextBox.h"
#include "Button.h"
#include "../Logging/Logging.h"
#include <fstream>
#include <memory>

bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp)
{
	if (sprite.contains(mp)) {
		return true;
	}
	return false;
}

void TypeBB(const std::string& textBox, sf::Font& font, std::shared_ptr<Logger> logger)
{
	int choice = 0;

	sf::RenderWindow window2;
	window2.create(sf::VideoMode(600, 600), "Game Type");

	Button button1("singleplayer.png", window2, { 80, 250 }, font, logger);
	Button button2("multiplayer.png", window2, { 320, 250 }, font, logger);
	Button button3("collab.jpg", window2, { 80, 250 }, font, logger);
	Button button4("opponents.jpg", window2, { 320, 250 }, font, logger);

	sf::Text text;
	text.setFillColor(sf::Color::White);
	text.setPosition({ 250, 140 });
	text.setCharacterSize(20);
	text.setFont(font);

	button1.SetText("Singleplayer", sf::Color::White, { 100, 200 }, font);
	button2.SetText("Multiplayer", sf::Color::White, { 350, 200 }, font);
	button3.SetText("Friends", sf::Color::White, { 100, 200 }, font);
	button4.SetText("Opponents", sf::Color::White, { 350, 200 }, font);

	sf::Vector2f poz;
	poz.x = sf::Mouse::getPosition(window2).x;
	poz.y = sf::Mouse::getPosition(window2).y;

	TextBox textBox1(15, sf::Color::White, false, 10, font);
	textBox1.SetPosition({ 100, 100 });
	textBox1.SetText("Player's 2 name:", sf::Color::White, { 90, 60 }, font);

	while (window2.isOpen())
	{
		sf::Event event;

		while (window2.pollEvent(event))
		{
			if (isSpriteHover(button1.GetSprite().getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
			{
				if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
				{
					GameBB game(false, false);
					game.SetPlayer1Name(textBox, logger);
					logger->log("BB singlePlayer start", Logger::Level::Info);
					game.Run();
					logger->log("BB singlePlayer end", Logger::Level::Info);
					game.UpdatePlayer1(logger);
					window2.close();
				}
			}
			else
				if (isSpriteHover(button2.GetSprite().getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
				{
					if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
					{
						while (window2.isOpen())
						{
							sf::Event event2;
							window2.clear(sf::Color::Black);

							if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && textBox1.GetSelected())
							{
								textBox1.SetSelected(false);
								text.setString("Hello, " + textBox1.GetText() + "!");
							}

							while (window2.pollEvent(event2))
							{
								if (event2.type == sf::Event::Closed)
									window2.close();

								if (event2.type == sf::Event::TextEntered)
									textBox1.TypedOn(event2);

								if (textBox1.IsPressed(sf::Vector2f(event2.mouseButton.x, event2.mouseButton.y)))
									textBox1.SetSelected(true);

								if (isSpriteHover(button3.GetSprite().getGlobalBounds(), sf::Vector2f(event2.mouseButton.x, event2.mouseButton.y)) == true)
								{
									if (event2.type == sf::Event::MouseButtonReleased && event2.mouseButton.button == sf::Mouse::Left)
									{
										GameBB game(true, false);

										game.SetPlayer1Name(textBox, logger);
										game.SetPlayer2Name(textBox1.GetText(), logger);
										logger->log("BB multiPlayer Friends start", Logger::Level::Info);
										game.Run();
										logger->log("BB multiPlayer Friends end", Logger::Level::Info);
										game.UpdatePlayer1(logger);
										game.UpdatePlayer2(logger);
										window2.close();
									}
								}
								else
									if (isSpriteHover(button4.GetSprite().getGlobalBounds(), sf::Vector2f(event2.mouseButton.x, event2.mouseButton.y)) == true)
									{
										if (event2.type == sf::Event::MouseButtonReleased && event2.mouseButton.button == sf::Mouse::Left)
										{
											GameBB game(true, true);
											game.SetPlayer1Name(textBox, logger);
											game.SetPlayer2Name(textBox1.GetText(), logger);
											logger->log("BB multiPlayer Opponents start", Logger::Level::Info);
											game.Run();
											logger->log("BB multiPlayer Opponents end", Logger::Level::Info);
											game.UpdatePlayer1(logger);
											game.UpdatePlayer2(logger);
											window2.close();
										}
									}
							}
							window2.clear(sf::Color::Black);

							textBox1.Draw(window2);

							window2.draw(button3.GetSprite());
							window2.draw(button4.GetSprite());
							window2.draw(button3.GetText());
							window2.draw(button4.GetText());

							window2.draw(textBox1.GetDescription());
							window2.draw(text);

							window2.display();
						}
					}
				}
		}
		window2.clear(sf::Color::Black);

		window2.draw(button1.GetSprite());
		window2.draw(button2.GetSprite());

		window2.draw(button1.GetText());
		window2.draw(button2.GetText());

		window2.display();
	}
}

int main()
{
	std::ofstream of("syslog.log");
	of.close();
	of.open("syslog.log", std::ios::app);

	std::shared_ptr <Logger> logger = std::make_shared<Logger>(of);
	logger->log("Started Aplication...", Logger::Level::Info);

	sf::RenderWindow window;
	window.create(sf::VideoMode(600, 600), "Welcome to the Game!");

	static sf::Font font;
	if (!font.loadFromFile("arial.ttf"))
	{
		logger->log("Font not loaded", Logger::Level::Error);
	}

	Button button1("button1.jpg", window, { 80, 250 }, font, logger);
	Button button2("button2.png", window, { 320, 250 }, font, logger);
	button1.SetText("Brick Breaker", sf::Color::Red, { 100, 200 }, font);
	button2.SetText("Pong", sf::Color::Blue, { 380, 200 }, font);

	sf::Vector2f mp;
	mp.x = sf::Mouse::getPosition(window).x;
	mp.y = sf::Mouse::getPosition(window).y;

	TextBox textBox(15, sf::Color::White, false, 10, font);
	textBox.SetPosition({ 100, 100 });
	textBox.SetText("Player's 1 name:", sf::Color::White, { 90, 60 }, font);

	sf::Text text;
	text.setFillColor(sf::Color::White);
	text.setPosition({ 250, 140 });
	text.setCharacterSize(20);
	text.setFont(font);

	while (window.isOpen())
	{
		sf::Event event;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && textBox.GetSelected()) {
			textBox.SetSelected(false);
			text.setString("Hello, " + textBox.GetText() + "!");
		}

		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::TextEntered)
				textBox.TypedOn(event);

			if (textBox.IsPressed(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)))
				textBox.SetSelected(true);

			if (isSpriteHover(button1.GetSprite().getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
			{
				if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
					TypeBB(textBox.GetText(), font, logger);
			}
			else
				if (isSpriteHover(button2.GetSprite().getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
				{
					if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
					{
						GameP game;
						game.SetPlayer1Name(textBox.GetText(), logger);
						logger->log("Pong start", Logger::Level::Info);
						game.Run();
						logger->log("Pong end", Logger::Level::Info);
						game.UpdatePlayer1(logger);
					}
				}
		}
		window.clear(sf::Color::Black);

		textBox.Draw(window);

		window.draw(button1.GetText());
		window.draw(button2.GetText());
		window.draw(textBox.GetDescription());
		window.draw(text);

		window.draw(button1.GetSprite());
		window.draw(button2.GetSprite());

		window.display();
	}

	logger->log("Ended Aplication.", Logger::Level::Info);

	return 0;
}