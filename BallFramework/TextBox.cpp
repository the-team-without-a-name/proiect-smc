#include "TextBox.h"
#include <iostream>

constexpr auto BoxRatioX = 4;
constexpr auto BoxRatioY = 13;

void TextBox::InputLogic(int charTyped)
{
	if (charTyped != DELETE_KEY && charTyped != ENTER_KEY && charTyped != ESCAPE_KEY) {
		m_stream << static_cast<char>(charTyped);
	}
	else
		if (charTyped == DELETE_KEY) {
			if (m_stream.str().length() > 0) {
				DeleteLastChar();
			}
		}
	m_textBox.setString(m_stream.str() + "_");
}

void TextBox::DeleteLastChar()
{
	std::string t = m_stream.str();
	std::string newT = "";

	for (int i = 0; i < t.length() - 1; ++i)
		newT += t[i];

	m_stream.str("");
	m_stream << newT;
	m_textBox.setString(m_stream.str());
}

TextBox::TextBox(int size, sf::Color color, bool sel, int limit, sf::Font& FONT)
{
	m_textBox.setCharacterSize(size);
	m_textBox.setFillColor(color);
	m_textBox.setFont(FONT);
	m_isSelected = sel;
	m_limit = limit;

	if (sel)
		m_textBox.setString("_");
	else
		m_textBox.setString("");

	m_box.setOutlineColor(sf::Color::Green);
	m_box.setOutlineThickness(2);
	m_box.setFillColor(sf::Color::Transparent);
	m_box.setSize({ static_cast<float>(BoxRatioY * limit), static_cast<float>(BoxRatioX * limit) });
}

void TextBox::SetPosition(sf::Vector2f pos)
{
	m_textBox.setPosition(pos);
	m_box.setPosition(pos.x - 10, pos.y - 10);
}

void TextBox::SetLimit(int lim)
{
	m_limit = lim;
}

void TextBox::SetSelected(bool sel)
{
	m_isSelected = sel;
	if (!sel) {
		m_textBox.setString(m_stream.str());
	}
}

std::string TextBox::GetText() const
{
	return m_stream.str();
}

sf::Text TextBox::GetDescription() const
{
	return m_text;
}

bool TextBox::GetSelected() const
{
	return m_isSelected;
}

void TextBox::Draw(sf::RenderWindow& window) const
{
	window.draw(m_box);
	window.draw(m_textBox);
}

void TextBox::TypedOn(sf::Event input)
{
	if (m_isSelected) {
		int charTyped = input.text.unicode;
		if (charTyped < 128) {
			if (m_stream.str().length() < m_limit) {
				InputLogic(charTyped);
			}
			else
				if (m_stream.str().length() == m_limit && charTyped == DELETE_KEY) {
					DeleteLastChar();
					m_textBox.setString(m_stream.str() + "_");
				}
		}
	}
}

bool TextBox::IsPressed(sf::Vector2f pos) const
{
	sf::FloatRect bounds = m_box.getGlobalBounds();
	if (bounds.contains(pos))
		return true;
	return false;
}

void TextBox::SetText(const std::string& text, sf::Color color, sf::Vector2f pos, sf::Font& FONT)
{
	m_text.setString(text);
	m_text.setFillColor(color);
	m_text.setPosition(pos);
	m_text.setCharacterSize(CHARACTER_SIZE);
	m_text.setFont(FONT);
}