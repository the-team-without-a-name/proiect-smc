#include "PaddleBB.h"
#include "Paddle.h"
#include <SFML/Graphics.hpp>
#include "Game.h"

PaddleBB::PaddleBB(float width, float lenght, std::pair<int, int> coord, sf::Color color)
	:Paddle(width, lenght, coord, color)
{
	m_paddle.setFillColor(color);
}

void PaddleBB::Move(sf::Vector2f movement)
{
	sf::Vector2f coord = m_paddle.getPosition();
	if (coord.x + m_paddle.getSize().x < Game::WIDTH && coord.x > 0)
	{
		m_paddle.move(movement);
	}
	else
		if (coord.x + m_paddle.getSize().x >= Game::WIDTH && movement.x != Paddle::SPEED)
		{
			m_paddle.move(movement);
		}
		else
			if (coord.x <= 0 && movement.x != Paddle::SPEED * (-1))
			{
				m_paddle.move(movement);
			}
}

void PaddleBB::ChangeSize(float size)
{
	m_paddle.setSize({ m_paddle.getSize().x + size * m_paddle.getSize().x / 2, m_paddle.getSize().y });
}

bool PaddleBB::GetLeftMove() const
{
	return m_movingLeft;
}

bool PaddleBB::GetRightMove() const
{
	return m_movingRight;
}

void PaddleBB::SetLeftMove(bool left)
{
	m_movingLeft = left;
}

void PaddleBB::SetRightMove(bool right)
{
	m_movingRight = right;
}