#include "Game.h"
#include "PaddleBB.h"
#include "LevelBB.h"

class GameBB : public Game
{
public:
	GameBB(bool multiplayer, bool opponents);
	void Run() override;

	void SetPlayer1Name(const std::string& name, std::shared_ptr<Logger> logger) override;
	void SetPlayer2Name(const std::string& name, std::shared_ptr<Logger> logger) override;

	void UpdatePlayer1(std::shared_ptr<Logger> logger) override;
	void UpdatePlayer2(std::shared_ptr<Logger> logger) override;

private:
	void ProcessEvents();
	void Update() override;
	void Render(bool& gameOver) override;
	void HandlePlayerInput(sf::Keyboard::Key key, bool isPressed) override;

	LevelBB m_level;
	std::pair<Player, PaddleBB>m_player;
	std::pair<Player, PaddleBB>m_player2;
	bool player1Won = false;
	bool player2Won = false;
};