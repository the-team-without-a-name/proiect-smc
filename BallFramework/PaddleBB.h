#pragma once
#include "Paddle.h"

class PaddleBB : public Paddle
{
public:
	PaddleBB() = default;
	PaddleBB(float lenght, float width, std::pair<int, int> coord, sf::Color color);

	bool GetLeftMove() const;
	bool GetRightMove() const;

	void SetLeftMove(bool left);
	void SetRightMove(bool right);

	void Move(sf::Vector2f movement);
	void ChangeSize(float size) override;

private:
	bool m_movingLeft = 0, m_movingRight = 0;
};